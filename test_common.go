package broute

import (
	"bytes"
	"context"
	"math/rand"
	"testing"
	"time"
)

func testSendAndReceiveAllPerms(t *testing.T, all ...Network) {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	for _, from := range all {
		for _, to := range all {
			doTestSendAndRecieve(t, from, to, ctx)
		}
	}
}

var randomSource = rand.NewSource(0)

func randomBytes(size int) []byte {
	bytes := make([]byte, size)
	for i := range bytes {
		bytes[i] = byte(randomSource.Int63() & 0xff)
	}
	return bytes
}

func testSendAndReceive(t *testing.T, from, to Network) {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	doTestSendAndRecieve(t, from, to, ctx)
}

func doTestSendAndRecieve(t *testing.T, from, to Network, ctx context.Context) {
	t.Logf("testing send from %v to %v\n", from.LocalAddress(), to.LocalAddress())
	dataOut := randomBytes(16)

	go func() {
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			default:
				err := from.WritePacket(DataPacket{to.LocalAddress(), dataOut})
				if err != nil {
					panic(err)
				}
				time.Sleep(100 * time.Millisecond)
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			t.Error("didn't get expected packet before timeout")
			return
		default:
			packetIn, _ := to.ReadPacketContext(ctx)
			if bytes.Equal(dataOut, packetIn.Data) {
				t.Logf("sent ok from %v to %v\n", from.LocalAddress(), to.LocalAddress())
				return
			}
		}
	}
}
