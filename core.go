package broute

import (
	"context"
)

type Address struct {
	Id string
}

type Network interface {
	ReadPacketContext(ctx context.Context) (DataPacket, error)
	WritePacket(packet DataPacket) error
	LocalAddress() Address
	AddLink(link Link)
}

type Link interface {
	RemoteAddress() Address
	ReadLinkPacket() ([]byte, error)
	WriteLinkPacket([]byte) error
}

type Router interface {
	AddLink(addr Address, outgoing chan<- RoutePacket, incoming <-chan RoutePacket)
	NextHop(addr Address) Address
}

type DataPacket struct {
	Dest Address
	Data []byte
}

type RoutePacket struct {
	Data []byte
}

func toBytes(in interface{}) (bytes []byte) {
	switch p := in.(type) {
	case DataPacket:
		bytes = make([]byte, 9+len(p.Data))
		bytes[0] = 0 // 0 means data packet
		copy(bytes[1:9], []byte(p.Dest.Id))
		copy(bytes[9:], p.Data)
		return
	case RoutePacket:
		bytes = []byte{1} // 1 means routing broadcast packet
		bytes = append(bytes, []byte(p.Data)...)
		return
	default:
		panic("unknown packet type")
	}
}

func fromBytes(bytes []byte) interface{} {
	switch bytes[0] {
	case 0:
		return DataPacket{Dest: Address{Id: string(bytes[1:9])}, Data: bytes[9:]}
	case 1:
		return RoutePacket{Data: bytes[1:]}
	default:
		panic("implement me")
	}
}
