package broute

import (
	"log"
	"sync"
)

func newStaticRouter(myAddress Address, routes map[Address]Address, l *log.Logger) *staticRouter {
	r := &staticRouter{
		myAddress: myAddress,
		routes:    routes,
		newLinks:  make(chan newRouteLink),
		incoming:  make(chan newRoutePacket),
		log:       l,
	}
	go r.rloop()
	return r
}

type staticRouter struct {
	myAddress Address
	rLock     sync.RWMutex
	routes    map[Address]Address

	newLinks chan newRouteLink
	incoming chan newRoutePacket
	log      *log.Logger
}

func (r *staticRouter) AddLink(addr Address, outgoing chan<- RoutePacket, incoming <-chan RoutePacket) {
	r.newLinks <- newRouteLink{addr, outgoing, incoming}
}

func (r *staticRouter) NextHop(addr Address) Address {
	r.rLock.RLock()
	defer r.rLock.RUnlock()
	return r.routes[addr]
}

func (r *staticRouter) rloop() {

	for {
		select {
		case <-r.incoming:
			panic("this should never happen with static routing")
		case l := <-r.newLinks:
			close(l.outgoing)
			r.rLock.Lock()
			r.routes[l.linkAddr] = l.linkAddr
			r.rLock.Unlock()

			go func() {
				for x := range l.incoming {
					r.incoming <- newRoutePacket{x, l.linkAddr}
				}
			}()
		}
	}
}
