package broute

type PipeLink struct {
	addr Address
	in   <-chan []byte
	out  chan<- []byte
}

func (pl *PipeLink) ReadLinkPacket() ([]byte, error) {
	return <-pl.in, nil
}

func (pl *PipeLink) WriteLinkPacket(p []byte) error {
	select {
	case pl.out <- p:
	default:
		panic("overflowed buffer")
	}
	return nil
}

func (pl *PipeLink) RemoteAddress() Address {
	return pl.addr
}

func NewPipeLinks(a, b Address) (Link, Link) {
	c1 := make(chan []byte, 1024)
	c2 := make(chan []byte, 1024)

	return &PipeLink{a, c1, c2}, &PipeLink{b, c2, c1}
}
