package broute

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

func TestDataPacket(t *testing.T) {
	dp := DataPacket{Dest: Address{Id: "01234567"}, Data: []byte{1, 2, 3, 4, 5, 6, 7}}
	b := toBytes(dp)
	dp2 := fromBytes(b).(DataPacket)

	if dp.Dest != dp2.Dest {
		t.Fatalf("address mis-match: %v %v", dp.Dest, dp2.Dest)
	}

	if !bytes.Equal(dp.Data, dp2.Data) {
		t.Fatal("data mis-match")
	}
}
func TestSimple(t *testing.T) {
	// 1 <-->  2 <--> 3

	a1 := Address{"00000001"}
	a2 := Address{"00000002"}
	a3 := Address{"00000003"}
	net1_to_net2, net2_to_net1 := NewPipeLinks(a2, a1)
	net2_to_net3, net3_to_net2 := NewPipeLinks(a3, a2)

	l1 := log.New(os.Stderr, fmt.Sprintf("%v : ", a1), log.LstdFlags)
	router1 := newStaticRouter(a1, map[Address]Address{a3: a2}, l1)
	net1 := NewNetwork(a1, router1, l1)
	net1.AddLink(net1_to_net2)

	l2 := log.New(os.Stderr, fmt.Sprintf("%v : ", a2), log.LstdFlags)
	router2 := newStaticRouter(a2, map[Address]Address{}, l2)
	net2 := NewNetwork(a2, router2, l2)
	net2.AddLink(net2_to_net1)
	net2.AddLink(net2_to_net3)

	l3 := log.New(os.Stderr, fmt.Sprintf("%v : ", a3), log.LstdFlags)
	router3 := newStaticRouter(a3, map[Address]Address{a1: a2}, l3)
	net3 := NewNetwork(a3, router3, l3)
	net3.AddLink(net3_to_net2)

	time.Sleep(100 * time.Millisecond)

	testSendAndReceiveAllPerms(t, net1, net2, net3)
}
