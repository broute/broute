package broute

import (
	"encoding/json"
	"log"
	"sync"
	"time"
)

type newRoutePacket struct {
	packet   RoutePacket
	linkAddr Address
}

type newRouteLink struct {
	linkAddr Address
	outgoing chan<- RoutePacket
	incoming <-chan RoutePacket
}

type routeEntry struct {
	Addr Address
	Hops int
}

func newBCSRouter(myAddress Address, l *log.Logger) *bcsRouter {
	r := &bcsRouter{
		myAddress: myAddress,
		routes:    make(map[Address]routeEntry),
		incoming:  make(chan newRoutePacket),
		newLinks:  make(chan newRouteLink),
		outgoing:  make(map[Address]chan<- RoutePacket),
		log:       l,
	}
	go r.rloop()
	return r
}

type bcsRouter struct {
	myAddress Address
	rLock     sync.RWMutex
	routes    map[Address]routeEntry

	newLinks chan newRouteLink
	incoming chan newRoutePacket
	outgoing map[Address]chan<- RoutePacket
	log      *log.Logger
}

func (r *bcsRouter) AddLink(addr Address, outgoing chan<- RoutePacket, incoming <-chan RoutePacket) {
	r.newLinks <- newRouteLink{addr, outgoing, incoming}
}

func (r *bcsRouter) NextHop(addr Address) Address {
	r.rLock.RLock()
	defer r.rLock.RUnlock()
	return r.routes[addr].Addr
}

func (r *bcsRouter) rloop() {
	needToBroadcast := make(chan struct{}, 1)
	needToBroadcast <- struct{}{}

	t := time.NewTimer(0)

	var doRB chan struct{}

	for {
		select {
		case <-t.C:
			doRB = needToBroadcast
		case <-doRB:
			doRB = nil
			t.Reset(20 * time.Millisecond)
			for la, linkOut := range r.outgoing {
				linkOut <- RoutePacket{ser(routeEntry{Addr: r.myAddress, Hops: 0})}

				// send on to all other routes.
				r.rLock.RLock()
				for to, re := range r.routes {
					if la != re.Addr {
						//r.log.Printf("telling link %v about route to %v\n", la, re)
						linkOut <- RoutePacket{ser(routeEntry{Addr: to, Hops: re.Hops + 1})}
					}
				}
				r.rLock.RUnlock()
			}
		case rp := <-r.incoming:
			re := unser(rp.packet.Data)
			linkAddr := rp.linkAddr

			if re.Addr == r.myAddress {
				r.log.Printf("ignoring request to add route to myself from link %v\n", linkAddr)
				continue
			}
			r.rLock.Lock()
			if r.routes[re.Addr] != (routeEntry{}) /*|| net.outgoing[addr] != nil*/ {
				r.rLock.Unlock()
				r.log.Printf("already have route for %v. ignoring\n", re.Addr)
			} else {
				// save route
				r.routes[re.Addr] = routeEntry{Addr: linkAddr, Hops: re.Hops}
				r.rLock.Unlock()
				r.log.Printf("added route to %v via %v\n", re.Addr, linkAddr)
				//r.log.Printf("routes is %v\n", r.routes)

				select {
				case needToBroadcast <- struct{}{}:
				default:
					// already flagged
				}
			}
		case l := <-r.newLinks:
			r.outgoing[l.linkAddr] = l.outgoing
			go func() {
				for x := range l.incoming {
					r.incoming <- newRoutePacket{x, l.linkAddr}
				}
			}()
		}
	}
}

func unser(in []byte) routeEntry {
	var re routeEntry
	err := json.Unmarshal(in, &re)
	if err != nil {
		panic(err)
	}
	return re
}

func ser(re routeEntry) []byte {
	s, _ := json.Marshal(re)
	return s
}
