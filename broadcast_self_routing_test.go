package broute

import (
	"fmt"
	"log"
	"os"
	"testing"
)

func TestBroadcastSelfRoutingLinearE2E(t *testing.T) {
	// 0 <--> .... <--> n

	count := 30

	a := make([]Address, count)
	nets := make([]Network, count)

	for i := 0; i < count; i++ {
		a[i] = Address{fmt.Sprintf("%08d", i)}
	}

	for i := 0; i < count; i++ {
		logger := log.New(os.Stderr, fmt.Sprintf("%v : ", a[i]), log.LstdFlags)
		nets[i] = NewNetwork(a[i], newBCSRouter(a[i], logger), logger)

		if i > 0 {
			l, r := NewPipeLinks(a[i-1], a[i])
			nets[i-1].AddLink(r)
			nets[i].AddLink(l)
		}

	}

	testSendAndReceive(t, nets[0], nets[count-1])
}

func TestBroadcastSelfRoutingLinearAll(t *testing.T) {
	// 0 <--> 1 <--> 2 <--> 3 <--> 4 <--> 5

	count := 6

	a := make([]Address, count)
	nets := make([]Network, count)

	for i := 0; i < count; i++ {
		a[i] = Address{fmt.Sprintf("%08d", i)}
	}

	for i := 0; i < count; i++ {
		logger := log.New(os.Stderr, fmt.Sprintf("%v : ", a[i]), log.LstdFlags)
		nets[i] = NewNetwork(a[i], newBCSRouter(a[i], logger), logger)

		if i > 0 {
			l, r := NewPipeLinks(a[i-1], a[i])
			nets[i-1].AddLink(r)
			nets[i].AddLink(l)
		}

	}

	testSendAndReceiveAllPerms(t, nets...)
}

func TestBroadcastSelfRoutingSquareOppositeCorners(t *testing.T) {
	//  0 <-->  1 <-->  2 <-->  3
	//  |       |       |       |
	//  4 <-->  5 <-->  6 <-->  7
	//  |       |       |       |
	//  8 <-->  9 <--> 10 <--> 11
	//  |       |       |       |
	// 12 <--> 13 <--> 14 <--> 15

	dx := 4
	dy := 4

	a := make([]Address, dx*dy)
	nets := make([]Network, dx*dy)

	for i := 0; i < dx*dy; i++ {
		a[i] = Address{fmt.Sprintf("%08d", i)}
	}

	for y := 0; y < dy; y++ {

		for x := 0; x < dx; x++ {
			node := (y * dx) + x

			logger := log.New(os.Stderr, fmt.Sprintf("%v : ", a[node]), log.LstdFlags)
			nets[node] = NewNetwork(a[node], newBCSRouter(a[node], logger), logger)

			if x > 0 {
				l, r := NewPipeLinks(a[node-1], a[node])
				nets[node-1].AddLink(r)
				nets[node].AddLink(l)
			}

			if y > 0 {
				l, r := NewPipeLinks(a[node-dx], a[node])
				nets[node-dx].AddLink(r)
				nets[node].AddLink(l)
			}

		}
	}

	testSendAndReceive(t, nets[0], nets[(dx*dy)-1])
	//	testSendAndReceiveAllPerms(t, nets...)
}
