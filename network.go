package broute

import (
	"context"
	"errors"
	"fmt"
	"log"
)

type defaultNetwork struct {
	myAddress Address
	incoming  chan DataPacket
	unrouted  chan DataPacket
	links     map[Address]Link
	router    Router
	log       *log.Logger
}

func (net *defaultNetwork) ReadPacketContext(ctx context.Context) (DataPacket, error) {
	select {
	case p, ok := <-net.incoming:
		if !ok {
			return DataPacket{}, errors.New("incoming closed. TODO: return proper error")
		}
		return p, nil
	case <-ctx.Done():
		return DataPacket{}, ctx.Err()
	}
}

func (net *defaultNetwork) WritePacket(p DataPacket) error {
	net.unrouted <- p
	return nil
}

func (net *defaultNetwork) LocalAddress() Address {
	return net.myAddress
}

func NewNetwork(myAddress Address, router Router, l *log.Logger) Network {
	net := &defaultNetwork{
		myAddress: myAddress,
		incoming:  make(chan DataPacket, 16),
		unrouted:  make(chan DataPacket, 16),
		links:     make(map[Address]Link),
		router:    router,
		log:       l,
	}

	go net.loop()
	return net
}

func (net *defaultNetwork) AddLink(link Link) {
	net.links[link.RemoteAddress()] = link

	routesIn := make(chan RoutePacket)
	routesOut := make(chan RoutePacket)
	net.router.AddLink(
		link.RemoteAddress(),
		routesOut,
		routesIn,
	)

	go func() {
		for rp := range routesOut {
			_ = link.WriteLinkPacket(toBytes(rp))
		}
	}()

	go func(link Link) {
		for {
			p, err := link.ReadLinkPacket()
			if err != nil {
				panic(err)
			}
			dp := fromBytes(p)
			switch p := dp.(type) {
			case DataPacket:
				net.unrouted <- p
			case RoutePacket:
				routesIn <- p
			default:
				panic("implement me")
			}
		}
	}(link)

}

func (net *defaultNetwork) loop() {
	emptyAddr := Address{}
	for {
		select {
		case out := <-net.unrouted:
			//net.log.Printf("unrouted %v\n", out)

			// destination is me. route to local
			if out.Dest == net.myAddress {
				//net.log.Printf("delivering locally %v\n", out)
				net.incoming <- out
				continue
			}

			nextHop := net.router.NextHop(out.Dest)

			if nextHop == emptyAddr {
				//panic("broken routing")
				net.log.Printf("%v : no next hop to %v. dropping\n", net.myAddress, out.Dest)
				//net.log.Printf("routes is %v\n", net.routes)
				continue
			}
			link := net.links[nextHop]

			net.log.Printf("sending to link %v : %v\n", nextHop, out)
			if link == nil {
				panic(fmt.Sprintf("nil link. [%v] next hop is %v", net.myAddress, nextHop))
			}
			err := link.WriteLinkPacket(toBytes(out))
			if err != nil {
				panic(err)
			}
		}
	}
}
